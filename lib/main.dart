import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLight(){
    return
      ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.blue,
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.blue,
            iconTheme: IconThemeData(
              color: Colors.white,
            )
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }
}

var appbar = AppBar(
  backgroundColor: Colors.blue,
  title: Text("Profile Page"),
  leading: Icon(Icons.arrow_back),
  actions: <Widget>[
    IconButton(
      icon: Icon(Icons.star_border),
      onPressed: () {
        print("Contact is Start");
      },
    ),
  ],
);

var body = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,

          //Height constraint at Container widget level
          height: 250,

          child: Image.network(
            "https://lyricstranslate.com/files/styles/artist/public/20158908_011.jpg",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Kim Chaewon",
                  style: TextStyle(fontSize: 30),
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: Colors.grey,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Theme (
            data: ThemeData(
              iconTheme: IconThemeData(
                color: Colors.green
              )
            ),
            child: profileActionItem(),)


        ),
        Divider(
          color: Colors.black87,
        ),

        mobilePhoneListTile(),
        Divider(
          color: Colors.black87,
        ),

        otherPhoneListTile(),
        Divider(
          color: Colors.black87,
        ),

        emailListTile(),

        Divider(
          color: Colors.black87,
        ),

        addressListTile(),



      ],
    ),
  ],
);
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
      ? MyAppTheme.appThemeLight()
          :MyAppTheme.appThemeDark(),

      home: Scaffold(
        appBar: appbar,
        body: body,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}


Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//Listtile
Widget mobilePhoneListTile(){
  return ListTile(
    leading:Icon(Icons.call) ,
    title: Text("088-888-8888"),
    subtitle: Text("mobile"),
    trailing:IconButton(
      icon: Icon(Icons.message),
      // color: Colors.pink,
      onPressed: (){},
    ) ,
  );

}

Widget otherPhoneListTile(){
  return ListTile(
    leading:Text(""),
    title: Text("099-999-9999"),
    subtitle: Text("other"),
    trailing:IconButton(
      icon: Icon(Icons.message),
      // color: Colors.pink,
      onPressed: (){},
    ) ,
  );

}
Widget emailListTile(){
  return ListTile(
    leading:Icon(Icons.email) ,
    title: Text("NongMoo123@gmail.com"),
    subtitle: Text("Work"),

  );

}

Widget addressListTile(){
  return ListTile(
    leading:Icon(Icons.location_on) ,
    title: Text("821 Sakaeo St,Sakaeo"),
    subtitle: Text("home"),
    trailing:IconButton(
      icon: Icon(Icons.turn_right_outlined),
      // color: Colors.pink,
      onPressed: (){},
    ) ,
  );

}
Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton(),

    ],
  );
}
